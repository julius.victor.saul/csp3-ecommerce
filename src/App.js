import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar.js'
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import Logout from './pages/Logout.js'
import Products from './pages/Products.js'
import Admin from './pages/Admin.js'
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import './App.css';
import { UserProvider } from './UserContext'

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value ={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar />
          <Routes>
            {/*<Route exact path="/" element={<Home/>} />*/}
            <Route exact path="/" element={<Register/>} />
            <Route exact path="/register" element={<Register/>} />
            <Route exact path="/login" element={<Login/>} />
            <Route exact path="/products" element={<Products/>} />
            <Route exact path="/admin" element={<Admin/>} />
            <Route exact path="/logout" element={<Logout/>} />
          </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
