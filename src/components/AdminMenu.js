// import { useState } from 'react';
// Proptypes - used to validate props
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Image } from "react-bootstrap";
import { CartCheckFill } from 'react-bootstrap-icons'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';
import Accordion from 'react-bootstrap/Accordion'
import Form from 'react-bootstrap/Form'
import UserContext from '../UserContext';

export default function AdminMenu({courseProp}) {
	//console.log(props);
	const {user, setUser} = useContext(UserContext);
	const {_id, productName, description, price} = courseProp;
	const [admin, setAdmin] = useState('')
	const [adminPrice, setAdminPrice] = useState(price)

	

	function deleteProduct(){
		fetch(`https://csp3database.herokuapp.com/product/${_id}`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.text())
		.then(data => {
			console.log(data)
		})
	}

	function updateProduct() {
		fetch(`https://csp3database.herokuapp.com/product/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization : `Bearer ${localStorage.token}`
			},
			body: JSON.stringify({
				price: adminPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})

	}


	

    return (
    	<div id="cards-div">
    		<Row>
    			<Col lg={4}>{_id}</Col>
    			<Col lg={3}>{productName}</Col>
    			<Col lg={3}>
    				<Form>
    					<Form.Control onChange={e => setAdminPrice(e.target.value)} type="text" value={adminPrice} />
    				</Form>
    			</Col>
    			<Col lg={1}><Button onClick={() => updateProduct()} variant="primary">Udate</Button></Col>
    			<Col lg={1}><Button onClick={() => deleteProduct()} variant="danger">Delete</Button></Col>
    		</Row>
    	</div>
    )
}

