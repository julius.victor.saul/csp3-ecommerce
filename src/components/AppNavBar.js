import { Fragment, useContext } from 'react';
// Import necessary components from react-bootstrap
import { useState, useEffect } from 'react';
import Offcanvas from 'react-bootstrap/Offcanvas'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav';
import { Navigate, useNavigate } from 'react-router-dom';
import Form from 'react-bootstrap/Form'
import { Link, NavLink } from 'react-router-dom';
import { CartCheckFill } from 'react-bootstrap-icons';
import UserContext from '../UserContext';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import FormControl from 'react-bootstrap/FormControl'
import Cart from '../pages/Cart';
import Swal from 'sweetalert2';



export default function AppNavBar(){
	const navigate = useNavigate();
	const { user, setUser } = useContext(UserContext);

	const [show, setShow] = useState(false);
	const [orders, setOrders] = useState([]);
	const [bill, setBill] = useState([])
	const [totalPrice, setTotalPrice] = useState(0)

	const reducer = (previousValue, currentValue) => previousValue + currentValue;
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	const showCart = () => cartMenu();


	useEffect(() => {
		if(totalPrice === 0){
			getBill();
		}
	})

	function updateCart() {
		fetch("https://csp3database.herokuapp.com/order/myOrders")
		.then(res => res.json())
		.then(data =>{
			console.log(data);
			setOrders(data.map(order => {
					return (
						<Cart key={order.id} courseProp={order}/>
					)
				})
			)
		})
	}

	function getBill() {
		fetch("https://csp3database.herokuapp.com/order/myOrders")
		.then(res => res.json())
		.then(data =>{
			console.log("Hello");
			setBill(data.map(order => {
				return order.totalAmount
			}))
			console.log(bill)
			if(bill.length !== 0) {
				setTotalPrice(bill.reduce(reducer))
			}
		})
	}

	function checkout() {
		Swal.fire({
			title: `Total: ${totalPrice}`,
			icon: "success"
		})
		handleClose();
		setUser({id: null});
		
		navigate("/login");
		
	}

	function cartMenu() {
		getBill();
		updateCart();
		handleShow();
	}
	

	return (
		<Navbar id="navbar" bg="primary" className="p-2" variant="dark">
			<Navbar.Brand href="/">Cartee</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
			<Nav className="ml-auto">
				<Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
				{(user.id !== null) ? 
					<Fragment>
						{(user.isAdmin === true) ?
							<Nav.Link as={NavLink} to="/admin" exact>Admin</Nav.Link>
							:
							<Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
						}
						<Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
						<Button className='ml-4' variant="warning" onClick={cartMenu}>Cart</Button>
					</Fragment>
					:
					<Fragment>
						<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
						<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
					</Fragment>
				}
			</Nav>
			</Navbar.Collapse>
			<Offcanvas placement="end" show={show} onHide={handleClose}>
				<Offcanvas.Header closeButton>
					<Offcanvas.Title id='icon'>Cart</Offcanvas.Title>
				</Offcanvas.Header>
				<Offcanvas.Body>
					{orders}
			    </Offcanvas.Body>
			    <Offcanvas.Body>
			    	{totalPrice}
				</Offcanvas.Body>
				<Container>
			    	<Button id='form-button' onClick={checkout} variant="danger">Checkout</Button>
			    </Container>
			</Offcanvas>
		</Navbar>
	)
}
