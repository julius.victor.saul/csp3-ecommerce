// import { useState } from 'react';
// Proptypes - used to validate props
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext, Fragment } from 'react';
import { Image } from "react-bootstrap";
import { CartCheckFill } from 'react-bootstrap-icons'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';
import Accordion from 'react-bootstrap/Accordion'
import Form from 'react-bootstrap/Form'
import ToastContainer from 'react-bootstrap/ToastContainer'
import Toast from 'react-bootstrap/Toast'
import product from '../assets/product.png'
import Swal from 'sweetalert2';



export default function ProductMenu({productProp}) {
	const {_id, productName, price, description, quantity} = productProp;	
	// State hook - used to keep track of information related to individual components
	// Syntax: const [getter, setter] = useState(initialGetterValue);
	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(30);
	const [cartQuantity, setCartQuantity] = useState(1);
	const [name, setName] = useState('');
	const [id, setId] = useState('');
	const [productPrice, setProductPrice] = useState(0);
	const [show, setShow] = useState(false)
	const [order, setOrder] = useState({})

	const toggleShow = () => setShow(!show)

	function Checkout(){
		fetch('https://csp3database.herokuapp.com/order/checkout', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: productName,
				id: _id,
				price: price
			})
		})
		.then(res => res.text())
		.then(data => {
			console.log(data)
			toggleShow();
		})
	}

	function updateOrder(){
		fetch('https://csp3database.herokuapp.com/order/add/', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: productName,
				orderPrice: price,
				quantity: cartQuantity
			})
		})
		.then(res => res.text())
		.then(data => {
			console.log(data)
			toggleShow();
		})
	}

	function checkOrder(e) {
		e.preventDefault(e);

		fetch('https://csp3database.herokuapp.com/order/check', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				productId: _id
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
    	    		title: "Product already on Cart",
    	            icon: "error",
    	            text: "Adjust quantity in Cart"
    	        })
			} else {
				Checkout();
			}
		})
	}

    return (
    	<Fragment>
    		<ToastContainer id='toast' className="p-3">
    		    <Toast show={show} onClose={toggleShow} delay={3000} autohide>
    		        <Toast.Header closeButton={false}>
    		            <strong className="me-auto">Cart</strong>
    		            <small>Just now</small>
    		        </Toast.Header>
    		        <Toast.Body>Added {productName}</Toast.Body>
    		    </Toast>
    		</ToastContainer>
    		<Container id='product-card' fluid>
    			<Card id='card'>
    				<Card.Img as="img" variant="top" src={product} alt="card-image"/>
    				<Card.Body>
    					<Card.Title>{productName}</Card.Title>
    			        <Card.Text>Description: {_id}</Card.Text>
    			      	<Card.Text>Price: {price}</Card.Text>
    			      	<Button onClick={(e) => checkOrder(e)} id="card-button" type="submit" variant="success">Add to cart</Button>
    			    </Card.Body>
    			</Card>
    		</Container>
    	</Fragment>
    )
}

