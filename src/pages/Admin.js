import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext, Fragment } from 'react';
import { Image } from "react-bootstrap";
import AdminMenu from '../components/AdminMenu';
import '../App.css'
import { CartCheckFill } from 'react-bootstrap-icons'
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Accordion from 'react-bootstrap/Accordion'
import Form from 'react-bootstrap/Form'

export default function Admin() {

	const { user } = useContext(UserContext);
	const [data, setData] = useState([]);
	const [item, setItem] = useState([])
	const [name, setName] = useState('')
	const [priceAdmin, setPriceAdmin] = useState(0)
	const [desc, setDesc] = useState('')

	useEffect(() => {
		if(user.isAdmin === false){
			
		}

		fetch("https://csp3database.herokuapp.com/product")
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setData(data.map(product => {
					return (
						<AdminMenu key={product.id} courseProp={product}/>
					)
				})
			)
		})
	}, [])

	function addProduct(e) {
		fetch('https://csp3database.herokuapp.com/product', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization : `Bearer ${localStorage.token}`
			},
			body: JSON.stringify({
				productName: name,
				description: desc,
				price: priceAdmin
			})
		})
		.then(res => res.json())
		.then(data => {

		})
	}



	return (
		<Container id="bg" fluid>
			<div id="product-bg">
				<CartCheckFill id="product-logo" />
				<h1 id="product-title">Cartee</h1>
			</div>
			<div className="">
				<h1 className="m-5">Manage Products</h1>
				<div>
					<Accordion>
						<Accordion.Item eventKey="1">
					    	<Accordion.Header>Add Product</Accordion.Header>
					    	<Accordion.Body>
					    		<Form onSubmit={(e) => addProduct(e)}>
					    		  	<Form.Group className="mb-3">
					    		    	<Form.Label>Product Name</Form.Label>
					    		    	<Form.Control value={name} onChange={ e => setName(e.target.value)} type="text" placeholder="Example: Apple" />
					    		  	</Form.Group>

					    		  	<Form.Group className="mb-3">
					    		    	<Form.Label>Product Price</Form.Label>
					    		    	<Form.Control value={priceAdmin} onChange={ e => setPriceAdmin(e.target.value)} type="number" placeholder="Example: 100" />
					    		  	</Form.Group>

					    		  	<Form.Group className="mb-3">
					    		    	<Form.Label>Description</Form.Label>
					    		    	<Form.Control value={desc} onChange={ e => setDesc(e.target.value)} type="text" placeholder="Example: Ripe" />
					    		  	</Form.Group>

					    		  	<Button variant="primary" type="submit">
					    		    	Submit
					    		  	</Button>
					    		</Form>
					    	</Accordion.Body>
						</Accordion.Item>
					</Accordion>
				</div>
				<div id="cards-div">
					<Row>
						<Col lg={4}>Product ID</Col>
						<Col lg={3}>Product Name</Col>
						<Col lg={3}>Price</Col>
						<Col lg={1}>Update</Col>
						<Col lg={1}>Delete</Col>
					</Row>
				</div>
				<Container fluid id="card-admin">
					<Row lg={4}>
						{data}
					</Row>	
				</Container>
			</div>
		</Container>
	)
}


