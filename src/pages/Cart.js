import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Image } from "react-bootstrap";
import { CartCheckFill } from 'react-bootstrap-icons'
import ProductMenu from '../components/ProductMenu';
import Accordion from 'react-bootstrap/Accordion'
import Form from 'react-bootstrap/Form'
import '../App.css'
import '../font/SFPRODISPLAYBOLD.OTF'; 
import product from '../assets/product.png'
import Swal from 'sweetalert2';

export default function Cart({courseProp}) {

	
	const {_id, totalAmount, products, productId, orderQuantity, price} = courseProp;
	const [isActive, setIsActive] = useState(false);
	const [data, setData] = useState([]);
	const [cartQuantity, setCartQuantity] = useState(orderQuantity);
	const [isNegative, setIsNegative] = useState(true)
	const [bill, setBill] = useState(0)
	let quantityCart = () => setCartQuantity(orderQuantity)
	const incrementCart = () => setCartQuantity(cartQuantity + 1);
	let decrementCart = () => setCartQuantity(cartQuantity - 1)

	useEffect(() => {
		if( cartQuantity <= 0 ) {
			setIsNegative(false);
		}
		if( cartQuantity >=1 ) {
			setIsNegative(true);
		}
		fetch(`https://csp3database.herokuapp.com/order/add/${_id}`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				totalAmount: price*cartQuantity,
				orderQuantity: cartQuantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
	})


	function deleteProduct() {
		fetch("https://csp3database.herokuapp.com/order", {
			method: "DELETE",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				id: _id
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			Swal.fire({
    	    		title: "Product deleted",
    	            icon: "error"
    	    })
    	    setIsNegative(true)
		})
	}


	return (
		<Card>
		    <Card.Img variant="top" src={product}/>
		    <Card.Body>
		      	<Card.Title value={products} >{products}</Card.Title>
		      	<Card.Text>Description: {productId}</Card.Text>
		      	<Card.Text >Price: {price*cartQuantity}</Card.Text>
		      	<Form>
		      		<Form.Group>
		      			<Row>
		      				<Col>
		      					<Form.Label>
		      						Quantity:
		      					</Form.Label>
		      				</Col>
		      				<Col>
		   						<Button onClick={incrementCart} variant="success">+</Button>
		      				</Col>
		      				<Col>
		      					<Form.Control type="text" value={cartQuantity} placeholder={orderQuantity} />
		      				</Col>
		      				<Col>
		      					{ isNegative ?
		      						<Button onClick={decrementCart} variant="danger">-</Button>
		      						:
		      						<Button onClick={deleteProduct} variant="danger">Delete?</Button>
		   						}
		      				</Col>
		      			</Row>
		      		</Form.Group>
		      	</Form>
		    </Card.Body>
		</Card>
	)
}