import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import Form from 'react-bootstrap/Form'
import { Link } from 'react-router-dom';
import { Navigate, useNavigate } from 'react-router-dom';
import { CartCheckFill } from 'react-bootstrap-icons'
import UserContext from '../UserContext';
import '../App.css'
import Swal from 'sweetalert2';

export default function Login() {
	const navigate = useNavigate();
	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
    // Validate to enable submit button when all fields are populated and both passwords match
    	if ((email !== '' && password1 !== '') && (password1 === password1))
    	{
    	  setIsActive(true);
    	}
    	else{
    	  setIsActive(false);
    	}
  	}, [ email, password1])

    function authenticate(e){
    	e.preventDefault();

    	fetch('https://csp3database.herokuapp.com/user/login', {
    		method: 'POST',
    		headers: {
    			'Content-Type': 'application/json'
    		},
    		body: JSON.stringify({
    			email: email,
    			password: password1
    		})
    	})
    	.then(res => res.json())
    	.then(data => {
    		console.log(data);
    		// If no user information is found, the "access" property will not be available and will return undefined
    	    if(typeof data.access !== "undefined"){
    	    // The token will be used to retrieve user information across the whole frontend application and storing it in the localStorage to allow ease of access to the user's information
    	        localStorage.setItem('token', data.access);
    	        retrieveUserDetails(data.access);

    	        Swal.fire({
    	        	title: "Login Successful",
    	            icon: "success"
    	        })
    	        navigate("/products");
    	        
    	    }
    	    else {
    	    	Swal.fire({
    	    		title: "Authentication failed",
    	            icon: "error",
    	            text: "Check you login details and try again."
    	        })
    	    }
    	})

    	setEmail('');
    	setPassword1('');

    }

    const retrieveUserDetails = (token) => {
    	fetch('https://csp3database.herokuapp.com/user/details', {
    		headers: {
    			Authorization: `Bearer ${ token }`
    		}
    	})
    	.then(res => res.json())
        .then(data => {
        	console.log(data);

        	setUser({
        		id: data._id,
        		isAdmin: data.isAdmin
        	})
        })
    }

    
    

	return (
			(user.id !== null) ?
			    <Navigate to="/" />
			:
			<Container fluid>
				<Row>
					<Col md={7} lg={8} id="logo-col">
						<div id="logo-bg">
							<div id="logo-div">
								<CartCheckFill id="logo" />
								<h1 id="logo-title">Cartee</h1>
							</div>
						</div>
					</Col>
					<Col md={5} lg={4} className="mb-3" >
						<div id="form-div">
							<Form id="form" onSubmit={(e) => authenticate(e)}>
								<h1 id="form-title" >Login</h1>
							  	<Form.Group controlId="formRegister">
							    	<Form.Label className="mb-0">Email address</Form.Label>
							    	<Form.Control
							    	id="form-input"  
							    	type="email" 
							    	value={email} 
							    	placeholder="Enter email"
							    	onChange={ e => setEmail(e.target.value)}
							    	/>
							  	</Form.Group>

							  	<Form.Group controlId="formRegister">
							    	<Form.Label className="mb-0" >Password</Form.Label>
							    	<Form.Control
							    	id="form-input"
							    	type="password" 
							    	value={password1} 
							    	placeholder="Password"
							    	onChange={ e => setPassword1(e.target.value)} 
							    	/>
							  	</Form.Group>

							  	<div id="form-button-div">
							  		{ isActive ?
							  				<Button  id="form-button" type="submit">
							  				  	Login
							  				</Button>
							  			:
							  			// Disabled state
							  			<Button  id="form-button" variant="danger" type="submit" disabled>
							  				  Login
							  			</Button>
							  		}
							  	</div>
							</Form>
						</div>
					</Col>
				</Row>
			</Container>	
		)
}

