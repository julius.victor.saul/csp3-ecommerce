import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Image } from "react-bootstrap";
import { CartCheckFill } from 'react-bootstrap-icons'
import ProductMenu from '../components/ProductMenu';
import Accordion from 'react-bootstrap/Accordion'
import Form from 'react-bootstrap/Form'
import '../App.css'
import Toast from 'react-bootstrap/Toast'
import ToastContainer from 'react-bootstrap/ToastContainer'
import Stack from 'react-bootstrap/Stack'

export default function Products() {

	const [isActive, setIsActive] = useState(false);
	const [productName, setProductName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [productData, setProductData] = useState([]);
    const [orderData, setOrderData] = useState([]);

    useEffect(() => {
    	fetch("https://csp3database.herokuapp.com/product")
    	.then(res => res.json())
    	.then(data =>{
    		setProductData(data.map(product => {
    				return (
    					<ProductMenu key={product.id} productProp={product}/>
    				)
    			})
    		)
    	})
    }, [])




	return (
		<Container id="bg" fluid>
			{/*<ToastContainer delay={3000} autohide id="toast" className="p-3" position="top-end">
			    <Toast>
			        <Toast.Header closeButton={false}>
			        	<img
			           		src="holder.js/20x20?text=%20"
			            	className="rounded me-2"
			            	alt=""
			        	/>
			        	<strong className="me-auto">Bootstrap</strong>
			        <small>11 mins ago</small>
			        </Toast.Header>
			        <Toast.Body>Hello, world! This is a toast message.</Toast.Body>
			    </Toast>
			</ToastContainer>*/}
			<div id="product-bg">
				<CartCheckFill id="product-logo" />
				<h1 id="product-title">Cartee</h1>
			</div>
			<Row>
				<Col lg={2}>
					<Accordion>
					  <Accordion.Item eventKey="0">
					    <Accordion.Header>Filter By</Accordion.Header>
					    <Accordion.Body>
					    	<Form.Check 
					    		type="checkbox"
					    		id="brand"
					    		label="Brand"
					    	/>
					    	<Form.Check 
					    		type="checkbox"
					    		id="price"
					    		label="Price"
					    	/>
					    	<Form.Check 
					    		type="checkbox"
					    		id="name"
					    		label="Name"
					    	/>
					    </Accordion.Body>
					  </Accordion.Item>
					</Accordion>
				</Col>
				<Col>
						<Container id='data-div' fluid>
								<Row lg={4} id='data'>
									{productData}
								</Row>
						</Container>
				</Col>
			</Row>
		</Container>
	)
}