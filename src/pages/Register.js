	import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext, Fragment } from 'react';
import Form from 'react-bootstrap/Form'
import { Link } from 'react-router-dom';
import { Redirect, useNavigate } from 'react-router-dom';
import { CartCheckFill } from 'react-bootstrap-icons'
import '../App.css'
import Login from '../pages/Login.js'
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {
	
	const navigate = useNavigate();
	const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [mobile, setMobile] = useState('');
    const [isActive, setIsActive] = useState(false);

    const {user, setUser} = useContext(UserContext);

    function registerUser(e) {

            // Prevents page redirection via form submission
            e.preventDefault();

            fetch('https://csp3database.herokuapp.com/user/checkEmail', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if(data === true){
                    Swal.fire({
                        title: 'Duplicate email found',
                        icon: 'error',
                        text: 'Please provide a different email.'   
                    });

                } else {

                    fetch('https://csp3database.herokuapp.com/user/register', {
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            email: email,
                            password: password1
                        })
                    })
                    .then(res => res.json())
                    .then(data => {

                        console.log(data);

                        if(data === true){

                            // Clear input fields
                            setEmail('');
                            setMobile('');
                            setPassword1('');
                            setPassword2('');

                            Swal.fire({
                                title: 'Registration successful',
                                icon: 'success',
                                text: 'Welcome to Zuitt!'
                            });

                            // Allows us to redirect the user to the login page after registering for an account
                            navigate("/login");

                        } else {

                            Swal.fire({
                                title: 'Something wrong',
                                icon: 'error',
                                text: 'Please try again.'   
                            });

                        };

                    })
                };

            })

        }


    useEffect(() => {
    // Validate to enable submit button when all fields are populated and both passwords match
    	if ((email !== '' && mobile !== ''  && password1 !== '' && password2 !== '') && (password1 === password2))
    	{
    	  setIsActive(true);
    	}
    	else{
    	  setIsActive(false);
    	}
  	}, [ email, mobile, password1, password2])
    
    /*function alert() {
    	Swal.fire({
    	  icon: 'success',
    	  iconColor: 'blue',
    	  title: 'Succesfully Registered',
    	  text: 'Click OK to proceed to Login'
    	})
    }*/

	return (
			<Container fluid>
				<Row>
					<Col md={7} lg={8} id="logo-col">
						<div id="logo-bg">
							<div id="logo-div">
								<CartCheckFill id="logo" />
								<h1 id="logo-title">Cartee</h1>
							</div>
						</div>
					</Col>
					<Col md={5} lg={4} className="mb-3" >
						<div id="form-div">
							<Form id="form" onSubmit={(e) => registerUser(e)}>
								<h1 id="form-title" >Register</h1>
							  	<Form.Group controlId="email">
							    	<Form.Label className="mb-0">Email address</Form.Label>
							    	<Form.Control
							    	id="form-input"  
							    	type="email" 
							    	value={email}
							    	placeholder="Enter email"
							    	onChange={ e => setEmail(e.target.value)}
							    	/>
							  	</Form.Group>

							  	<Form.Group controlId="mobile">
							    	<Form.Label className="mb-0">Mobile No.</Form.Label>
							    	<Form.Control
							    	id="form-input"
							    	type="text" 
							    	value={mobile} 
							    	placeholder="Mobile Number"
							    	onChange={ e => setMobile(e.target.value)}
							    	/>
							  	</Form.Group>

							  	<Form.Group controlId="password1">
							    	<Form.Label className="mb-0" >Password</Form.Label>
							    	<Form.Control
							    	id="form-input"
							    	type="password" 
							    	value={password1} 
							    	placeholder="Password"
							    	onChange={ e => setPassword1(e.target.value)} 
							    	/>
							  	</Form.Group>
							  	<Form.Group controlId="password2">
							    	<Form.Label className="mb-0">Verify Password</Form.Label>
							    	<Form.Control
							    	id="form-input"
							    	type="password" 
							    	value={password2} 
							    	placeholder="Verify Password"
							    	onChange={e => setPassword2(e.target.value)} 
							    	/>
							  	</Form.Group>

							  	<div id="form-button-div">
							  		{ isActive ?
							  			<Button id="form-button" type="submit">
							  				Register
							  			</Button>
							  			:
							  			// Disabled state
							  			<Button  id="form-button" variant="danger" type="submit" disabled>
							  				  Register
							  			</Button>
							  		}
							  	</div>
							</Form>
						</div>
					</Col>
				</Row>
			</Container>	
		)
}

